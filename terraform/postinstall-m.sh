#!/bin/bash
# Postinstall for Master Node

# Create User
useradd -s /bin/bash -c "Student" -m student
echo "Passw0rd" | passwd --stdin student
# Set sudo
echo "student ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers

# Adjust SSH
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
systemctl restart sshd
##

## K8S
yum -y install git jq
# Clone Repo
git clone https://gitlab.com/pety-linux/k8s/k8s-install.git
# Run K8S Installation Script
bash k8s-install/install-node-containerd.sh
##

## AWS Route53 stuff
# Variables
ip=`hostname -i`
zoneid=`aws route53 list-hosted-zones-by-name |  jq --arg name "pety.net." -r '.HostedZones | .[] | select(.Name=="\($name)") | .Id' | awk -F / '{ print $1 $3}'`
# Prepare config
cat <<EOF | tee dns.json
{
            "Comment": "CREATE/DELETE/UPSERT a record ",
            "Changes": [{
            "Action": "UPSERT",
                        "ResourceRecordSet": {
                                    "Name": "master.pety.net",
                                    "Type": "A",
                                    "TTL": 300,
                                 "ResourceRecords": [{ "Value": "${ip}"}]
}}]
}
EOF
# Apply
aws route53 change-resource-record-sets --hosted-zone-id ${zoneid} --change-batch file://dns.json
#

## Web server
yum -y install httpd
systemctl enable httpd --now

## Init K8S Cluster
kubeadm init --pod-network-cidr=10.244.0.0/16 | tee -a command
cat command |grep -A1 ^kubeadm > initcommand
cat initcommand  | bash
mv initcommand /var/www/html
rm command
# kubeconfig
mkdir -p /root/.kube
cp -i /etc/kubernetes/admin.conf /root/.kube/config
export KUBECONFIG=/etc/kubernetes/admin.conf
# CNI
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
##

## Install Helm
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
##

## Add aliases
cat <<EOF | tee -a /etc/bashrc
# Aliases
alias k=kubectl
alias c=clear
EOF
##
